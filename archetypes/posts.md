+++
title = "{{ replace .TranslationBaseName "-" " " | title }}"
date = "{{ .Date }}"
author = "Stephen M. Reaves"
authorTwitter = "rpscln" #do not include @
tags = ["", ""]
description = "<++>"
showFullContent = false
readingTime = false
hideComments = false
#draft = true
+++
